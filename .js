// The wrong way:

if (something === true)
	doAThing();
	
// Requirements change, now you need to do two things:

if (something === true)
	doAThing();
	doASecondThing();
	
/* You now have a bug, and potentially a vulnerability. doASecondThing will ALWAYS run, whether your if
 * statement is true or not. It's too easy to overlook this, and *will* go wrong at some point.
 * Case in point: https://www.imperialviolet.org/2014/02/22/applebug.html */
 
// The correct way:

if (something === true) {
	doAThing();
}

// Which, if requirements change, becomes:

if (something === true) {
	doAThing();
	doASecondThing();
}